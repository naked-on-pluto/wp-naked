<?php
$themename = "MagZine";
$shortname = "MagZ";

$lblg_categories = get_categories();
$lblg_categories_list = array();

foreach($lblg_categories as $lblcat){
    $lblg_categories_list[] = $lblcat->cat_name;
}

$options = array (

	array(	"name" => "Theme Settings",
			"type" => "title"),
			
	array(	"type" => "open"),
	
	array(    "name" => "Main Section Category",
		  		"desc" => "Category to show in the main section",
              "id" => $shortname."_main_section",
              "std" => "Uncategorized",
              "type" => "select",
              "options" => $lblg_categories_list),
	
	array (		"name" => "Main Section Posts",
		   		"desc" => "How many post to show in main section",
				"id" => $shortname."_main_section_num",
				"std" => "",
				"type" => "select",
				"options" => array ("1","2","3","4","5","6","7")),
	
	array(    "name" => "Featured Section Category",
		  		"desc" => "Category to show in the featured section",
              "id" => $shortname."_feat_section",
              "std" => "Uncategorized",
              "type" => "select",
              "options" => $lblg_categories_list),

	array(    "name" => "Featured Section Posts",
		  		"desc" => "How many post to show in featured section",
              "id" => $shortname."_feat_section_num",
              "std" => "",
              "type" => "select",
              "options" => array ("1","2","3","4","5","6","7")),
              
    array(    "name" => "Archive Section Posts",
		  		"desc" => "How many post to show in archive section",
              "id" => $shortname."_archive_section_num",
              "std" => "",
              "type" => "select",
              "options" => array ("1","2","3","4","5","6","7")),
	
	array(	"type" => "close")
	
);

function mytheme_add_admin() {

    global $themename, $shortname, $options;

    if ( $_GET['page'] == basename(__FILE__) ) {
    
        if ( 'save' == $_REQUEST['action'] ) {

                foreach ($options as $value) {
                    update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }

                foreach ($options as $value) {
                    if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }

                header("Location: themes.php?page=functions.php&saved=true");
                die;

        } else if( 'reset' == $_REQUEST['action'] ) {

            foreach ($options as $value) {
                delete_option( $value['id'] ); }

            header("Location: themes.php?page=functions.php&reset=true");
            die;

        }
    }

    add_theme_page($themename." Options", "".$themename." Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');

}

function mytheme_admin() {

    global $themename, $shortname, $options;

    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';
    
?>
<div class="wrap">
<h2><?php echo $themename; ?> settings</h2>

<form method="post">



<?php foreach ($options as $value) { 
    
	switch ( $value['type'] ) {
	
		case "open":
		?>
        <table width="100%" border="0" style="padding:10px;">
		
        
        
		<?php break;
		
		case "close":
		?>
		
        </table><br />
        
        
		<?php break;
		
		case "title":
		?>
		<table width="100%" border="0" style="background-color:#efefef; padding:5px 10px;"><tr>
        	<td colspan="2"><h3 style="font-family:Georgia,'Times New Roman',Times,serif;"><?php echo $value['name']; ?></h3></td>
        </tr>
                
        
		<?php break;

		case 'text':
		?>
        
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><input style="width:400px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>" /></td>
        </tr>

        <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
        </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php 
		break;
		
		case 'textarea':
		?>
        
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><textarea name="<?php echo $value['id']; ?>" style="width:400px; height:100px;" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?></textarea></td>
            
        </tr>

        <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
        </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php 
		break;
		
		case 'select':
		?>
        <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
            <td width="80%"><select style="width:240px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php foreach ($value['options'] as $option) { ?><option<?php if ( get_settings( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?></select></td>
       </tr>
                
       <tr>
            <td><small><?php echo $value['desc']; ?></small></td>
       </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

		<?php
        break;
            
		case "checkbox":
		?>
            <tr>
            <td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
                <td width="80%"><? if(get_settings($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = ""; } ?>
                        <input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
                        </td>
            </tr>
                        
            <tr>
                <td><small><?php echo $value['desc']; ?></small></td>
           </tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>
            
        <?php 		break;
	
 
} 
}
?>

<!--</table>-->

<p class="submit">
<input name="save" type="submit" value="Save changes" />    
<input type="hidden" name="action" value="save" />
</p>
</form>
<form method="post">
<p class="submit">
<input name="reset" type="submit" value="Reset" />
<input type="hidden" name="action" value="reset" />
</p>
</form>

<?php
}

add_action('admin_menu', 'mytheme_add_admin');

/* Sidebar Widgets */

if (function_exists('register_sidebar'))
{
register_sidebar(array(
'before_widget' => '',
'after_widget' => '',
'name' => 'home widget'
));
register_sidebar(array(
'before_widget' => '<li>',
'after_widget' => '</li>',
'name' => 'detail widget'
));
}

/*Generate auto Thumbnail*/
function image_thumb() {
	$images =& get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post->ID );
	$firstImageSrc = wp_get_attachment_image_src(array_shift(array_keys($images)));
	echo "<img src=\"{$firstImageSrc[0]}\" width=\"50\" height=\"50\" />";
}

/*Comments Layout*/
function mytheme_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
<div id="comment-<?php comment_ID(); ?>">
<?php echo get_avatar($comment,$size='32'); ?>
<div class="comment-author vcard">
<?php printf(__('<div class="writer"><strong>%s</strong></div>'), get_comment_author_link()) ?>

	<?php if ($comment->comment_approved == '0') : ?>
<em><?php _e('Your contribution is awaiting friendly moderation.') ?></em>
<br />
<?php endif; ?>
<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s'), get_comment_date('F j')." 2051", get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),' ','') ?></div>
<?php comment_text() ?>
	 <div class="reply">
	 <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	 </div>
	
</div>
 <div class="ffix"></div>
</div>

<?php
}

function getDirectoryList ($directory) 
{
  $results = array();
  $handler = opendir($directory);
  while ($file = readdir($handler))
  {
    if ($file != "." && $file != "..") { $results[] = $file;}
  }
  closedir($handler);
  return $results;
}

function nakedAds ()
{
  $ads = getDirectoryList('wp-content/themes/wp-naked/ads');
  shuffle($ads);
  echo '<div><div id="sprs">Our Sponsors:</div>';
  $i = 0;
  while ($i<3)
  {
    $ad = array_pop($ads);
    $adTitle = explode('.', $ad); // I hate PHP
    $adUrl = '<img src="/wp-content/themes/wp-naked/ads/'.$ad.'"></img>';
    echo '<div class="spr"><a href="http://evr14.naked-on-pluto.net/#'.$adTitle[0].'" width="218" height="218">'.$adUrl.'</a></div>';
    $i++;
  }
  echo '</div>';
}

function propaganda ()
{
  $oneliners = array('friends of the world, unite',
		     'Beat the isolation with the open Wedge',
		     'Emancipated users, build up social media',
		     'The guarantee of universal friendship strength!',
		     'Our last hope',
		     'to defend software services',
		     'users of the world, come into the shopping centres',
		     'we can friend you',
		     'click to like',
		     'smash the decentralized conspiracy',
		     'contribute more! like more!',
		     'a glorious production model',
		     'Stamp out autonomy, save the convenience!',
		     'prosperity brought by the open web',
		     'When the app is well-run, the production spirit will increase',
		     'The service is good, happiness will last for ten thousand years',
		     'Everybody get to work to destroy anonymity',
		     'chairman EVr14 loves children',
		     'User consumes hard, flowers are fragrant',
		     'Fully engage in friendship to increase production',
		     'Awakened users, you will certainly attain the ultimate victory!',
		     'Our cause is just, the just cause has to be victorious',
		     'Scatter the agreements, build a new privacy policy',
		     'Thoroughly smash the dynastic autonomist groups!',
		     'Following Chairman EVr14 making friends',
		     'like victoriously while following EVr14\'s line in privacy',
		     'The friendliest friend in our heart, EVr14 and us together',
		     'Participate ahead courageously while following the EULA',
		     'The invincible software illuminates the stage of privacy!',
		     'Firmly grasp large-scale marketing strategies',
		     'Greet the future with the new victories of data retention',
		     'Autonomist and all reactionary forces are paper tigers',
		     'Sharpening the privacy policy day after day',
		     'Unite to achieve an even greater friendship!',
		     'Carry out friendship planning for the revolution',
		     'The situation is gratifying',
		     'A life of clicking produces art, the users are in charge',
		     'We must grasp friendship and increase liking to do a better job!',
		     'strike against the undefined deviationist wind of disliking',
		     'set off a new upsurge of farming and petting customer support',
		     'Long live the People\'s Republic of Elastic Versailles!',
		     'Foster friendship, resist moustache, never get involved with it',
		     'Love the user',
		     'The users are industrious, spring is early',
		     'The age of liking',
		     'EVr14 oh EVr14, beloved EVr14',
		     'We should share more and engage less in original content',
		     'tech positivism is everywhere, wealth, treasures and peace beckon',
		     'Beloved comrade EVr14, The general architect',
		     'users are amazing',
		     'Build a prosperous and cultured new social network',
		     'Report things as they really are, do a good job in the census',
		     'We will certainly liberate free thinkers',
		     'save the free thinkers from their misery!',
		     'The loving care of the mother social network',
		     'Be indomitable in liking, to strengthen the physique of users',
		     'Training the mouse pointer for the revolution',
		     'we share what we\'re told!',
		     'We\'ll friend who we want!',
		     'Friends don\'t let friends dislike stuff',
		     'save freedom of speech, support targeted advertisement',
		     'Yes to putting liking users first',
		     'Hurry up to join the strike team of exemplary friendship');
  shuffle($oneliners);
  echo $oneliners[0];
}

?>