<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title('|', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<?php wp_head(); ?>
</head>

<body <?php if( is_home()) : ?> class="home" <?php endif; ?>>

<div id="outer"><!--outer-->
	<div id="container"><!--container-->
    	<div id="header">
        	<a href="/" id="title" alt=""><h1><?php bloginfo('name'); ?></h1></a>
		<div id="subtitle"><?php propaganda(); ?></div>
		<div id="infohead"><table><tr><td id="alttitle">the voice of elastic versailles r14</td><td id="date"><?php echo date('l jS \of F').' 2051'?><td></tr></table></div>
        </div>
        <!--<div class="ffix"></div>-->
        <div id="content"><!--content-->